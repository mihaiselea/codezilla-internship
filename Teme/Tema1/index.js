const express = require('express')
const lodash = require('lodash')
const axios = require('axios')
const fs = require('fs')
const Employee = require('./employees/employee');
const app = express();
const PORT = 3000;

const employees = new Array();
const promises = new Array();

axios.get('https://codezilla-assets.s3.eu-central-1.amazonaws.com/employees.json')
    .then((response) => {
        const data = lodash.filter(response.data, validateData);

        data.forEach((currentValue) => {
            const newEmployee = new Employee(currentValue.id,
                                            currentValue.name,
                                            currentValue.age,
                                            currentValue.abilities,
                                            currentValue.stamina);
            employees.push(newEmployee);

            promises.push(newEmployee.save());
        });

        const writingToTemp = new Promise((resolve, reject) => {
            fs.writeFile('./tmp/employees.json', JSON.stringify(data), (err) => {
                if (err)
                    reject(err)
                else
                    resolve('Succesfuly writing to file');
            });
        });

        writingToTemp.then((message) => {console.log(message)})
                    .catch((message) => {console.log(`Err writing to file: ${message}`)});

        /* Race -> saving only one file
        Promise.race(promises)
                    .then(values => console.log(values))
                    .catch(err => console.log(err));
        */
        Promise.all(promises)
                    .then(values => console.log(values))
                    .catch(err => console.log(err));
    })
    .catch(err => console.log(`Error requesting employees.json: ${err}`))


app.get('/employees', (_, res) => {
    res.json(employees)
})

app.get('/employees/sort', (_, res) => {
    const data = employees
                    .sort((a, b) => a.stamina - b.stamina)
                    .filter((object) => { return object.stamina < 75 });
    res.json(data);
});

app.get('/employees/leadership', (_, res) => {
    const data = employees
                    .filter((object) => { return object.abilities.includes('leadership')});

    res.json(data);
});

app.get('/employees/totalAge', (_, res) => {
    const totalAge = lodash.sumBy(employees, (obj) => { return obj.age });
    res.json({ "totalAge": totalAge })
})



app.listen(PORT, () => console.log(`App listen to port ${PORT}`))


// Validate DATA
function validateData(object) {
        // Validate Object ID >= 0
        if (object.id < 0 || object.id == null) return false;

        // Validate Object NAME => String, 2 <= ... <= 64
        if ( !(typeof object.name === 'string') || object.id == null) return false;
        if ( object.name.length < 2 || object.name.lenght > 64) return false;

        // Validate Object AGE >= 0
        if ( object.age < 0 || object.age == null) return false;

        // Validate STAMINA 1<= ... <= 100
        if (object.stamina < 1 || object.stamina > 100) return false;

        // Validate Object Abilities => String, 2 <= ... <= 64
        for(let i = 0; i < object.abilities.length; i++) {
            const obj = object.abilities[i];
            if (!(typeof obj === 'string')) return false;
            if (obj.length < 2 || obj.length > 64) return false;
        }
        return true;
}


