const { v4 } = require('uuid')
const fs = require('fs')

class Employee {
    constructor(id, name, age, abilities, stamina) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.abilities = abilities;
        this.stamina = stamina;

        this.INTERNAL_ID = v4();
    }

    async save(){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                fs.writeFile(`./employees/${this.INTERNAL_ID}.json`, JSON.stringify(this), (err) => {
                    if(err)
                        reject(`Err saving instance [${this.INTERNAL_ID}] to file : ${err}`)
                    else
                        resolve(`Saving object : ${this.INTERNAL_ID}`);
                })
            }, 500)
        })
    }

    getID() {
        return this.INTERNAL_ID;
    }



}

module.exports = Employee
