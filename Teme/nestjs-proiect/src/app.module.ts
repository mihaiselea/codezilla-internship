import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { BlogPostsModule } from './blog_posts/blog_posts.module';
import { CommentsModule } from './comments/comments.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticationModule } from './authentication/authentication.module';
import { JwtStrategy, JwtStrategyRefreshToken } from './authentication/jwt.strategy';

@Module({
  imports: [UsersModule, BlogPostsModule, CommentsModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'internship',
      autoLoadEntities: true,
      synchronize: true,
    }),
    AuthenticationModule,
  ],
  controllers: [AppController],
  providers: [AppService, JwtStrategy, JwtStrategyRefreshToken],
})
export class AppModule { }
