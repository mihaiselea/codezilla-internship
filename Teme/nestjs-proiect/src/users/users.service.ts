import { forwardRef, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Request } from 'express';
import { SignUpDTO } from 'src/authentication/dto/signup.dto';
import { BlogPostsService } from 'src/blog_posts/blog_posts.service';
import { CommentsService } from 'src/comments/comments.service';
import { Repository } from 'typeorm';
import { ChangeUserRoleDTO } from './dtos/change-role-user.dto';
import { CreateUserDTO, UserRoles } from './dtos/create-user.dto';
import { PutUserDTO } from './dtos/put-user.dto';
import { Users } from './users.entity';

@Injectable()
export class UsersService {


    async changeUserRole(id: number, body: ChangeUserRoleDTO, request: Request) {
        const admin = await this.getCurrentUser(request);

        if (admin.role != UserRoles.ADMIN) {
            throw new UnauthorizedException()
        }
        else {
            if (body.role) {
                return await this.usersRepository.createQueryBuilder()
                    .update(Users)
                    .set({ role: body.role, updatedAt: Date() })
                    .where("id = :id", { id: id })
                    .execute()
            }
        }
    }

    async getUserByEmail(email: string) {
        return await this.usersRepository.findOne({ email: email });
    }



    async signUpUser(body: SignUpDTO, salt: any, password: any) {
        const newUser = new Users();
        newUser.firstame = body.firstName;
        newUser.lastName = body.lastName;
        newUser.fullName = body.fullName;
        newUser.email = body.email;
        newUser.password = password;
        newUser.salt = salt;
        newUser.role = UserRoles.DEFAULT;
        newUser.profilePicture = "";

        newUser.createdAt = new Date();
        newUser.updatedAt = new Date();

        return await this.usersRepository.save(newUser);
    }

    async getUserCommentsAndBlogs(id: number) {
        return await this.usersRepository.findOne(id, {
            relations: ['blogPosts', 'comments']
        })
    }

    async putUser(id: number, body: PutUserDTO) {
        await this.usersRepository.createQueryBuilder()
            .update(Users)
            .set({
                firstame: body.firstame,
                lastName: body.lastName,
                fullName: body.fullName,
                email: body.email,
                password: body.password,
                salt: body.salt,
                role: body.role,
                profilePicture: body.profilePicture,
                updatedAt: new Date,
            })
            .where({ id: id })
            .execute();

    }

    async deleteUser(id: number) {
        return await this.usersRepository.delete({ id: id })
    }

    async createUser(body: CreateUserDTO) {
        const newUser = new Users();
        newUser.firstame = body.firstame;
        newUser.lastName = body.lastName;
        newUser.fullName = body.fullName;
        newUser.email = body.email;
        newUser.password = body.password;
        newUser.salt = body.salt;
        newUser.role = body.role;
        newUser.profilePicture = body.profilePicture;

        newUser.createdAt = new Date();
        newUser.updatedAt = new Date();

        return await this.usersRepository.save(newUser);
    }

    async getUserByID(id: number) {
        return await this.usersRepository.findOne({ id: id });
    }

    async getUsers() {
        return await this.usersRepository.find();
    }


    async getCurrentUser(request: Request) {
        const user: any = {
            ...request.user,
        }
        delete user.password;
        delete user.salt;
        return user;
    }

    constructor(
        @Inject(forwardRef(() => CommentsService))
        private commentsService: CommentsService,

        @Inject(forwardRef(() => BlogPostsService))
        private blogPostsService: BlogPostsService,

        @InjectRepository(Users)
        private usersRepository: Repository<Users>,
    ) { }
}
