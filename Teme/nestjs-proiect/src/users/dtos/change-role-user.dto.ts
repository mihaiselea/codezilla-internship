import { ApiProperty } from "@nestjs/swagger";
import { IsEnum } from 'class-validator';
import { UserRoles } from "./create-user.dto";

export class ChangeUserRoleDTO {
    @ApiProperty()
    @IsEnum(UserRoles)
    role: UserRoles;
}