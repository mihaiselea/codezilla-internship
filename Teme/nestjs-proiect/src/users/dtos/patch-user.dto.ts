import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsEnum } from 'class-validator';
import { UserRoles } from "./create-user.dto";

export class PatchUserDTO {

    @ApiPropertyOptional()
    firstame: string;

    @ApiPropertyOptional()
    lastName: string;

    @ApiPropertyOptional()
    fullName: string;

    @ApiPropertyOptional()
    email: string;

    @ApiPropertyOptional()
    password: string;

    @ApiPropertyOptional()
    @IsEnum(UserRoles)
    role: UserRoles;

    @ApiPropertyOptional()
    profilePicture: string;
}