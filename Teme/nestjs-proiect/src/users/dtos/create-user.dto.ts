import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from 'class-validator';

export enum UserRoles {
    DEFAULT = 'user',
    ADMIN = 'admin',
    PUBLISHER = 'publisher',
}

export class CreateUserDTO {

    id: number;

    @ApiProperty()
    @IsNotEmpty()
    firstame: string;

    @ApiProperty()
    @IsNotEmpty()
    lastName: string;

    @ApiProperty()
    @IsNotEmpty()
    fullName: string;

    @ApiProperty()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsNotEmpty()
    salt: string;

    @ApiProperty()
    @IsNotEmpty()
    role: UserRoles;

    @ApiProperty()
    @IsNotEmpty()
    profilePicture: string;


    createdAt: Date;

    updatedAt: Date;


}