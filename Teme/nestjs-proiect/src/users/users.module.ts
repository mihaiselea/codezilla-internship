import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogPostsModule } from 'src/blog_posts/blog_posts.module';
import { CommentsModule } from 'src/comments/comments.module';
import { UsersController } from './users.controller';
import { Users } from './users.entity';
import { UsersService } from './users.service';

@Module({
  imports: [forwardRef(() => CommentsModule),
  forwardRef(() => BlogPostsModule),
  TypeOrmModule.forFeature([Users])
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule { }
