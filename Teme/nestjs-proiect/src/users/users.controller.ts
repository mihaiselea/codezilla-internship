import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ChangeUserRoleDTO } from './dtos/change-role-user.dto';
import { CreateUserDTO } from './dtos/create-user.dto';
import { GetUserDTO } from './dtos/get-user.dto';
import { PutUserDTO } from './dtos/put-user.dto';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
@ApiBearerAuth()
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @UseGuards(AuthGuard('jwt'))
    @Get('me')
    async getCurrentUser(@Req() request: Request) {
        return this.usersService.getCurrentUser(request);
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch(':id/change-role')
    async changeUserRole(
        @Param() params: GetUserDTO,
        @Body() body: ChangeUserRoleDTO,
        @Req() request: Request
    ) {
        return this.usersService.changeUserRole(params.id, body, request);
    }

    @UseGuards(AuthGuard('refresh-token'))
    @Get('me')
    async getCurrentUserByRefreshToken(@Req() request: Request) {
        return this.usersService.getCurrentUser(request);
    }

    @Get()
    async getUsers() {
        return this.usersService.getUsers();
    }

    @Get(':id')
    async getUserByID(@Param() params: GetUserDTO) {
        return this.usersService.getUserByID(params.id);
    }

    @Post()
    async createUser(@Body() body: CreateUserDTO) {
        return this.usersService.createUser(body);
    }

    @Delete(':id')
    async deleteUser(@Param() params: GetUserDTO) {
        return this.usersService.deleteUser(params.id);
    }

    @Patch(':id')
    async patchUser(
        @Param() params: GetUserDTO,
        @Body() body: PutUserDTO,
    ) {
        this.usersService.putUser(params.id, body);
    }

    @Get(':id/CommentsAndBlogs')
    async getUserCommentsAndBlogs(
        @Param() params: GetUserDTO,
    ) {
        return this.usersService.getUserCommentsAndBlogs(params.id);
    }


}
