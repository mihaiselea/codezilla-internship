import { BlogPosts } from "src/blog_posts/blog_posts.entity";
import { Comments } from "src/comments/comments.entity";
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Users {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'first_name' })
    firstame: string;

    @Column({ name: 'last_name' })
    lastName: string;

    @Column({ name: 'full_name' })
    fullName: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    salt: string;

    @Column()
    role: string;

    @Column({ name: 'profile_picture' })
    profilePicture: string;

    @CreateDateColumn({ name: 'created_at' })
    createdAt: Date;

    @CreateDateColumn({ name: 'updated_at' })
    updatedAt: Date;

    @OneToMany(type => BlogPosts, blogPosts => blogPosts.user)
    blogPosts: BlogPosts[];

    @OneToMany(type => Comments, comments => comments.user)
    comments: Comments;

}