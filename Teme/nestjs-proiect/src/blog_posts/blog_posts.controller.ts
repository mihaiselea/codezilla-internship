import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { GetUserDTO } from 'src/users/dtos/get-user.dto';
import { BlogPostsService } from './blog_posts.service';
import { GetBlogPostsDTO } from './dtos/get-blog_posts.dto';
import { PublishBlogPostsDTO } from './dtos/publish-blog_post.dto';

@ApiTags('Blog Posts')
@Controller('blog-posts')
@ApiBearerAuth()
export class BlogPostsController {
    constructor(private readonly blogPostsService: BlogPostsService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    async getBlogPosts() {
        return this.blogPostsService.getBlogPosts();
    }

    @Get('/me')
    @UseGuards(AuthGuard('jwt'))
    async getCurrentUserBlogPost(
        @Req() request: Request) {
        return this.blogPostsService.getCurrentUserBlogPost(request);
    }

    @Get(':id')
    async getBlogPostsByID(
        @Param() params: GetBlogPostsDTO
    ) {
        return this.blogPostsService.getBlogPostsByID(params.id);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    async publishBlogPosts(
        @Body() body: PublishBlogPostsDTO,
        @Req() request: Request
    ) {
        return this.blogPostsService.publishBlogPosts(body, request);
    }




    @Get('/user/:id')
    async getBlogPostsOfUser(
        @Param() params: GetUserDTO,
        @Query() query: PaginatorQueryDTO
    ) {
        return this.blogPostsService.getBlogPostsOfUser(params.id, query);
    }

}

