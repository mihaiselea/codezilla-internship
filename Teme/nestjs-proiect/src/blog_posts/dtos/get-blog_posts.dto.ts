import { ApiProperty } from "@nestjs/swagger";

export class GetBlogPostsDTO {

    @ApiProperty()
    id: number;

}

