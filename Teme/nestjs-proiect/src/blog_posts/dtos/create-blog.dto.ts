import { ApiProperty } from "@nestjs/swagger";

export class CreateBlogPostsDTO {

    @ApiProperty()
    authorId: number;

    @ApiProperty()
    title: string;

    @ApiProperty()
    content: string;

    @ApiProperty()
    published: Date;

    @ApiProperty()
    createdAt: Date;

    @ApiProperty()
    updatedAt: Date;


    constructor(partial: Partial<CreateBlogPostsDTO>) {
        Object.assign(this, partial);
    }
}

