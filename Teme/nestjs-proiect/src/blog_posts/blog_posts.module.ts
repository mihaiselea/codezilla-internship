import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/users/users.module';
import { BlogPostsController } from './blog_posts.controller';
import { BlogPosts } from './blog_posts.entity';
import { BlogPostsService } from './blog_posts.service';

@Module({
  imports: [forwardRef(() => UsersModule),
  TypeOrmModule.forFeature([BlogPosts])
  ],

  controllers: [BlogPostsController],
  providers: [BlogPostsService],
  exports: [BlogPostsService]
})
export class BlogPostsModule { }
