import { forwardRef, Inject, Injectable, ParseIntPipe, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { UserRoles } from 'src/users/dtos/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { BlogPosts } from './blog_posts.entity';
import { CreateBlogPostsDTO } from './dtos/create-blog.dto';
import { PublishBlogPostsDTO } from './dtos/publish-blog_post.dto';


@Injectable()
export class BlogPostsService {
    async getCurrentUserBlogPost(request: Request) {
        const user: any = await this.usersService.getCurrentUser(request);
        const id = user.id;
        const newQuery = this.blogPostsRepository.createQueryBuilder('users').where('author_id = :id', { id });


        return await newQuery.getMany();
    }


    async publishBlogPosts(body: PublishBlogPostsDTO, request: Request) {

        const publisher = await this.usersService.getCurrentUser(request);

        if (publisher.role == UserRoles.DEFAULT) {
            throw new UnauthorizedException()
        }
        else {
            const newBlogPosts = new BlogPosts();

            newBlogPosts.user = await this.usersService.getUserByID(publisher.id);
            newBlogPosts.content = body.content;
            newBlogPosts.title = body.title;

            newBlogPosts.published = new Date();
            newBlogPosts.createdAt = new Date();
            newBlogPosts.updatedAt = new Date();

            //TODO Delete user password and salt

            return await this.blogPostsRepository.save(newBlogPosts);
        }
    }
    constructor(
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService,

        @InjectRepository(BlogPosts)
        private blogPostsRepository: Repository<BlogPosts>,
    ) { }

    async postBlogPosts(body: CreateBlogPostsDTO) {
        const newBlogPosts = new BlogPosts();
        newBlogPosts.user = await this.usersService.getUserByID(body.authorId);
        newBlogPosts.content = body.content;
        newBlogPosts.published = body.published;
        newBlogPosts.title = body.title;

        newBlogPosts.createdAt = new Date();
        newBlogPosts.updatedAt = new Date();

        return await this.blogPostsRepository.save(newBlogPosts);
    }


    async getBlogPosts() {
        return await this.blogPostsRepository.find();
    }

    async getBlogPostsByID(id: number) {
        return await this.blogPostsRepository.find({ id: id });
    }

    async getBlogPostsOfUser(id: number, query: PaginatorQueryDTO) {
        const newQuery = this.blogPostsRepository.createQueryBuilder('users').where('author_id = :id', { id });
        if (query.limit)
            newQuery.limit(query.limit);
        if (query.offset && query.limit)
            newQuery.offset(query.offset);

        return await newQuery.getMany();
    }
}    