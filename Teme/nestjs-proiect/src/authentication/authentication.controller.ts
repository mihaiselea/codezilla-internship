import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { AuthenticationService } from './authentication.service';
import { SignInDTO } from './dto/signin.dto';
import { SignUpDTO } from './dto/signup.dto';
import { Request } from 'express';


@ApiTags('authentication')
@Controller('authentication')
export class AuthenticationController {
    constructor(
        private readonly authenticationService: AuthenticationService,
    ) { }


    @Post('sign-up')
    async userSignup(@Body() body: SignUpDTO) {
        return this.authenticationService.userSignup(body);
    }

    @Post('sign-in')
    async userSignIn(@Body() body: SignInDTO) {
        return this.authenticationService.userSignIn(body);
    }

    @Get('accessToken')
    @UseGuards(AuthGuard('refresh-token'))
    async getAccessToken(@Req() request: Request) {
        return this.authenticationService.getAccessToken(request);
    }
}
