import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { SignUpDTO } from './dto/signup.dto';
import * as bcrypt from 'bcrypt';
import { SignInDTO } from './dto/signin.dto';
import * as jwt from 'jsonwebtoken';
import { Constants } from 'src/constants';
import { Request } from 'express';
@Injectable()
export class AuthenticationService {
    async getAccessToken(request: Request) {
        const user: any = this.usersSerivce.getCurrentUser(request);

        if (user) {
            const shortUser = {
                id: user.id,
                email: user.email,
            };

            const accesToken = jwt.sign(
                { ...shortUser },
                Constants.secretToken,
                {
                    // 1 hour
                    expiresIn: 60 * 60,
                },
            );

            const refreshToken = jwt.sign(
                { ...shortUser },
                Constants.secretToken,
                {
                    // 24 hours
                    expiresIn: '24h',
                },
            );

            return {
                ...shortUser,
                accesToken,
                refreshToken,
            };
        }
    }
    async userSignIn(body: SignInDTO) {
        const user = await this.usersSerivce.getUserByEmail(body.email);

        if (user) {
            const isValidPassord = await bcrypt.compare(body.password, user.password);

            if (isValidPassord) {
                const shortUser = {
                    id: user.id,
                    email: user.email,
                };

                const accesToken = jwt.sign(
                    { ...shortUser },
                    Constants.secretToken,
                    {
                        // 1 hour
                        expiresIn: 60 * 60,
                    },
                );

                const refreshToken = jwt.sign(
                    { ...shortUser },
                    Constants.secretToken,
                    {
                        // 24 hours
                        expiresIn: '24h',
                    },
                );

                return {
                    ...shortUser,
                    accesToken,
                    refreshToken,
                };
            }
        }

        throw new BadRequestException('Invalid Credentials');
    }
    async userSignup(body: SignUpDTO) {
        const salt = await bcrypt.genSalt();
        const password = await bcrypt.hash(body.password, salt);
        const user = this.usersSerivce.signUpUser(body, salt, password);
    }
    constructor(private readonly usersSerivce: UsersService) { }

}
