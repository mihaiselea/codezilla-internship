import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Constants } from 'src/constants';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly usersService: UsersService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: Constants.secretToken,
        });
    }

    async validate(payload: any) {
        const user = this.usersService.getUserByEmail(payload.email);

        if (user) {
            return user;
        }

        throw new UnauthorizedException();
    }
}

@Injectable()
export class JwtStrategyRefreshToken extends PassportStrategy(Strategy, 'refresh-token') {
    constructor(private readonly usersService: UsersService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: Constants.secretToken,
        });
    }

    async validate(payload: any) {
        const user = this.usersService.getUserByEmail(payload.email);

        if (user) {
            return user;
        }

        throw new UnauthorizedException();
    }
}
