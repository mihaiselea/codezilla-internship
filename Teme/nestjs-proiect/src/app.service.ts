import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return '<center><h1>Internship Codezilla NodeJS 2022</h1></center>';
  }
}
