export class Constants {
    // Genereaza aleatoriu secret token string
    static secretToken: string = (Math.random() + 1).toString(36).substring(7);
}