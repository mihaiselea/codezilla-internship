import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { GetUserDTO } from 'src/users/dtos/get-user.dto';
import { CommentsService } from './comments.service';
import { PublishCommentDTO } from './dto/publish-comment.dto';

@ApiTags('Comments')
@Controller('comments')
@ApiBearerAuth()
export class CommentsController {
    constructor(private readonly commentService: CommentsService) { }

    @Get()
    async getComments() {
        return this.commentService.getComments();
    }

    @Get('/user/:id')
    async getCommentsOfUser(
        @Param() params: GetUserDTO,
        @Query() query: PaginatorQueryDTO
    ) {
        return this.commentService.getCommentsOfUser(params.id, query);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    async publishComment(
        @Req() request: Request,
        @Body() body: PublishCommentDTO) {
        return this.commentService.publishComment(body, request);
    }
}
