import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Request } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { Comments } from './comments.entity';
import { PostCommentsDTO } from './dto/post-comments.dto';
import { PublishCommentDTO } from './dto/publish-comment.dto';


@Injectable()
export class CommentsService {
    async publishComment(body: PublishCommentDTO, request: Request) {

        const newComment = new Comments();
        newComment.content = body.content;
        newComment.contentType = body.content;
        newComment.replyToId = body.replyToId;
        newComment.createdAt = new Date();
        newComment.updatedAt = new Date();

        newComment.user = await this.usersService.getCurrentUser(request);

        return await this.commentsRepository.save(newComment);
    }
    constructor(
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService,

        @InjectRepository(Comments)
        private commentsRepository: Repository<Comments>,
    ) { }

    async postComment(body: PostCommentsDTO) {
        const newComment = new Comments();
        newComment.content = body.content;
        newComment.contentType = body.content;
        newComment.replyToId = body.replyToId;
        newComment.createdAt = new Date();
        newComment.updatedAt = new Date();

        newComment.user = await this.usersService.getUserByID(body.authorId);

        return await this.commentsRepository.save(newComment);
    }

    async getComments() {
        return await this.commentsRepository.find();
    }

    async getCommentsOfUser(id: number, query: PaginatorQueryDTO) {
        const newQuery = this.commentsRepository.createQueryBuilder('comments').where('author_id = :id', { id });
        if (query.limit)
            newQuery.limit(query.limit);
        if (query.offset && query.limit)
            newQuery.offset(query.offset);

        return await newQuery.getMany();
    }
}
