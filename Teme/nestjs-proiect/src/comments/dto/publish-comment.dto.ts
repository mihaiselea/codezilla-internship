import { ApiProperty } from "@nestjs/swagger";

export class PublishCommentDTO {
    @ApiProperty()
    replyToId: number;

    @ApiProperty()
    content: string;

    @ApiProperty()
    contentType: string;

}