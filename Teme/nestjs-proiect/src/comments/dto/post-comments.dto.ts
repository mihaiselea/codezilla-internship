import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class PostCommentsDTO {
    @ApiPropertyOptional()
    replyToId: number;

    @ApiProperty()
    content: string;

    @ApiProperty()
    contentType: string;

    @ApiProperty()
    authorId: number;
}