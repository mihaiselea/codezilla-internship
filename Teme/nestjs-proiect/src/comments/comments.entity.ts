import { Users } from "src/users/users.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Comments {
    @PrimaryGeneratedColumn()
    id: number;

    // TODO Add blogpostID


    // TODO relation to users 
    @Column({ name: 'reply_to_id' })
    replyToId: number;

    @Column()
    content: string;

    @Column({ name: 'content_type' })
    contentType: string;

    @CreateDateColumn({ name: 'created_at' })
    createdAt: Date;

    @CreateDateColumn({ name: 'updated_at' })
    updatedAt: Date;

    @ManyToOne(type => Users, user => user.comments)
    @JoinColumn({ name: 'author_id' })
    user: Users
}