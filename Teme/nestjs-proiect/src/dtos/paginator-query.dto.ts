import { ApiPropertyOptional } from "@nestjs/swagger";

export class PaginatorQueryDTO {
    @ApiPropertyOptional()
    limit: number;

    @ApiPropertyOptional()
    offset: number;
}