import { UserDTO } from "src/users/dtos/user.dto";
import { GetBlogPostsDTO } from "./get-blog-posts.dto";
export declare class GetBlogPostsByUserIDDTO {
    userDetail: UserDTO;
    blogs: GetBlogPostsDTO[];
    constructor(partial: Partial<GetBlogPostsByUserIDDTO>);
}
