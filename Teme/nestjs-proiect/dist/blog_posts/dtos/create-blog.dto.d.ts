export declare class CreateBlogPostsDTO {
    authorId: number;
    title: string;
    content: string;
    published: Date;
    createdAt: Date;
    updatedAt: Date;
    constructor(partial: Partial<CreateBlogPostsDTO>);
}
