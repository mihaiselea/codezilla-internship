export declare class CreateBlogPostsDTO {
    id: number;
    userID: number;
    blog: string;
    constructor(partial: Partial<CreateBlogPostsDTO>);
}
