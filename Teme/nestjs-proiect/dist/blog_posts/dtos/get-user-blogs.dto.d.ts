import { BlogPostsDTO } from "./blog-posts.dto";
declare enum gender {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
export declare class GetBlogPostsOfUser {
    id: number;
    name: string;
    age: number;
    gender: gender;
    blogs: BlogPostsDTO[];
    constructor(partial: Partial<GetBlogPostsOfUser>);
}
export {};
