export declare class PublishBlogPostsDTO {
    title: string;
    content: string;
}
