"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetBlogPostsOfUser = void 0;
var gender;
(function (gender) {
    gender["MASCULIN"] = "masculin";
    gender["FEMININ"] = "feminin";
})(gender || (gender = {}));
;
class GetBlogPostsOfUser {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
exports.GetBlogPostsOfUser = GetBlogPostsOfUser;
//# sourceMappingURL=get-user-blogs.dto.js.map