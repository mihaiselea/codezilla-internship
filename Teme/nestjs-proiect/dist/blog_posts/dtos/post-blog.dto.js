"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateBlogPostsDTO = void 0;
class CreateBlogPostsDTO {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
exports.CreateBlogPostsDTO = CreateBlogPostsDTO;
//# sourceMappingURL=post-blog.dto.js.map