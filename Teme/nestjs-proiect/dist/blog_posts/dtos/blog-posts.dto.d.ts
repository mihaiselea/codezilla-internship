export declare class BlogPostsDTO {
    id: number;
    userID: number;
    blog: string;
    constructor(partial: Partial<BlogPostsDTO>);
}
