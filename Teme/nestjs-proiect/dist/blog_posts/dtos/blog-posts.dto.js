"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogPostsDTO = void 0;
class BlogPostsDTO {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
exports.BlogPostsDTO = BlogPostsDTO;
//# sourceMappingURL=blog-posts.dto.js.map