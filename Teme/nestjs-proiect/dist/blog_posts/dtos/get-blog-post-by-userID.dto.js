"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetBlogPostsByUserIDDTO = void 0;
class GetBlogPostsByUserIDDTO {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
exports.GetBlogPostsByUserIDDTO = GetBlogPostsByUserIDDTO;
//# sourceMappingURL=get-blog-post-by-userID.dto.js.map