import { Users } from "src/users/users.entity";
export declare class BlogPosts {
    id: number;
    title: string;
    content: string;
    published: Date;
    createdAt: Date;
    updatedAt: Date;
    user: Users;
}
