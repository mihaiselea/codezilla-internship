"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogPostsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const create_user_dto_1 = require("../users/dtos/create-user.dto");
const users_service_1 = require("../users/users.service");
const typeorm_2 = require("typeorm");
const blog_posts_entity_1 = require("./blog_posts.entity");
let BlogPostsService = class BlogPostsService {
    constructor(usersService, blogPostsRepository) {
        this.usersService = usersService;
        this.blogPostsRepository = blogPostsRepository;
    }
    async getCurrentUserBlogPost(request) {
        const user = await this.usersService.getCurrentUser(request);
        const id = user.id;
        const newQuery = this.blogPostsRepository.createQueryBuilder('users').where('author_id = :id', { id });
        return await newQuery.getMany();
    }
    async publishBlogPosts(body, request) {
        const publisher = await this.usersService.getCurrentUser(request);
        if (publisher.role == create_user_dto_1.UserRoles.DEFAULT) {
            throw new common_1.UnauthorizedException();
        }
        else {
            const newBlogPosts = new blog_posts_entity_1.BlogPosts();
            newBlogPosts.user = await this.usersService.getUserByID(publisher.id);
            newBlogPosts.content = body.content;
            newBlogPosts.title = body.title;
            newBlogPosts.published = new Date();
            newBlogPosts.createdAt = new Date();
            newBlogPosts.updatedAt = new Date();
            return await this.blogPostsRepository.save(newBlogPosts);
        }
    }
    async postBlogPosts(body) {
        const newBlogPosts = new blog_posts_entity_1.BlogPosts();
        newBlogPosts.user = await this.usersService.getUserByID(body.authorId);
        newBlogPosts.content = body.content;
        newBlogPosts.published = body.published;
        newBlogPosts.title = body.title;
        newBlogPosts.createdAt = new Date();
        newBlogPosts.updatedAt = new Date();
        return await this.blogPostsRepository.save(newBlogPosts);
    }
    async getBlogPosts() {
        return await this.blogPostsRepository.find();
    }
    async getBlogPostsByID(id) {
        return await this.blogPostsRepository.find({ id: id });
    }
    async getBlogPostsOfUser(id, query) {
        const newQuery = this.blogPostsRepository.createQueryBuilder('users').where('author_id = :id', { id });
        if (query.limit)
            newQuery.limit(query.limit);
        if (query.offset && query.limit)
            newQuery.offset(query.offset);
        return await newQuery.getMany();
    }
};
BlogPostsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => users_service_1.UsersService))),
    __param(1, (0, typeorm_1.InjectRepository)(blog_posts_entity_1.BlogPosts)),
    __metadata("design:paramtypes", [users_service_1.UsersService, typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object])
], BlogPostsService);
exports.BlogPostsService = BlogPostsService;
//# sourceMappingURL=blog_posts.service.js.map