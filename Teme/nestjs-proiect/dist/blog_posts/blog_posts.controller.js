"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogPostsController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const paginator_query_dto_1 = require("../dtos/paginator-query.dto");
const get_user_dto_1 = require("../users/dtos/get-user.dto");
const blog_posts_service_1 = require("./blog_posts.service");
const get_blog_posts_dto_1 = require("./dtos/get-blog_posts.dto");
const publish_blog_post_dto_1 = require("./dtos/publish-blog_post.dto");
let BlogPostsController = class BlogPostsController {
    constructor(blogPostsService) {
        this.blogPostsService = blogPostsService;
    }
    async getBlogPosts() {
        return this.blogPostsService.getBlogPosts();
    }
    async getCurrentUserBlogPost(request) {
        return this.blogPostsService.getCurrentUserBlogPost(request);
    }
    async getBlogPostsByID(params) {
        return this.blogPostsService.getBlogPostsByID(params.id);
    }
    async publishBlogPosts(body, request) {
        return this.blogPostsService.publishBlogPosts(body, request);
    }
    async getBlogPostsOfUser(params, query) {
        return this.blogPostsService.getBlogPostsOfUser(params.id, query);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BlogPostsController.prototype, "getBlogPosts", null);
__decorate([
    (0, common_1.Get)('/me'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], BlogPostsController.prototype, "getCurrentUserBlogPost", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_blog_posts_dto_1.GetBlogPostsDTO]),
    __metadata("design:returntype", Promise)
], BlogPostsController.prototype, "getBlogPostsByID", null);
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [publish_blog_post_dto_1.PublishBlogPostsDTO, Object]),
    __metadata("design:returntype", Promise)
], BlogPostsController.prototype, "publishBlogPosts", null);
__decorate([
    (0, common_1.Get)('/user/:id'),
    __param(0, (0, common_1.Param)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_user_dto_1.GetUserDTO,
        paginator_query_dto_1.PaginatorQueryDTO]),
    __metadata("design:returntype", Promise)
], BlogPostsController.prototype, "getBlogPostsOfUser", null);
BlogPostsController = __decorate([
    (0, swagger_1.ApiTags)('Blog Posts'),
    (0, common_1.Controller)('blog-posts'),
    (0, swagger_1.ApiBearerAuth)(),
    __metadata("design:paramtypes", [blog_posts_service_1.BlogPostsService])
], BlogPostsController);
exports.BlogPostsController = BlogPostsController;
//# sourceMappingURL=blog_posts.controller.js.map