import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { GetUserDTO } from 'src/users/dtos/get-user.dto';
import { BlogPostsService } from './blog_posts.service';
import { GetBlogPostsDTO } from './dtos/get-blog_posts.dto';
import { PublishBlogPostsDTO } from './dtos/publish-blog_post.dto';
export declare class BlogPostsController {
    private readonly blogPostsService;
    constructor(blogPostsService: BlogPostsService);
    getBlogPosts(): Promise<import("./blog_posts.entity").BlogPosts[]>;
    getCurrentUserBlogPost(request: Request): Promise<import("./blog_posts.entity").BlogPosts[]>;
    getBlogPostsByID(params: GetBlogPostsDTO): Promise<import("./blog_posts.entity").BlogPosts[]>;
    publishBlogPosts(body: PublishBlogPostsDTO, request: Request): Promise<import("./blog_posts.entity").BlogPosts>;
    getBlogPostsOfUser(params: GetUserDTO, query: PaginatorQueryDTO): Promise<import("./blog_posts.entity").BlogPosts[]>;
}
