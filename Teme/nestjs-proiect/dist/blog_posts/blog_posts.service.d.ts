import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { BlogPosts } from './blog_posts.entity';
import { CreateBlogPostsDTO } from './dtos/create-blog.dto';
import { PublishBlogPostsDTO } from './dtos/publish-blog_post.dto';
export declare class BlogPostsService {
    private usersService;
    private blogPostsRepository;
    getCurrentUserBlogPost(request: Request): Promise<BlogPosts[]>;
    publishBlogPosts(body: PublishBlogPostsDTO, request: Request): Promise<BlogPosts>;
    constructor(usersService: UsersService, blogPostsRepository: Repository<BlogPosts>);
    postBlogPosts(body: CreateBlogPostsDTO): Promise<BlogPosts>;
    getBlogPosts(): Promise<BlogPosts[]>;
    getBlogPostsByID(id: number): Promise<BlogPosts[]>;
    getBlogPostsOfUser(id: number, query: PaginatorQueryDTO): Promise<BlogPosts[]>;
}
