export declare class PaginatorQueryDTO {
    limit: number;
    offset: number;
}
