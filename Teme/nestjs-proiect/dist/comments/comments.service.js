"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const users_service_1 = require("../users/users.service");
const typeorm_2 = require("typeorm");
const comments_entity_1 = require("./comments.entity");
let CommentsService = class CommentsService {
    constructor(usersService, commentsRepository) {
        this.usersService = usersService;
        this.commentsRepository = commentsRepository;
    }
    async publishComment(body, request) {
        const newComment = new comments_entity_1.Comments();
        newComment.content = body.content;
        newComment.contentType = body.content;
        newComment.replyToId = body.replyToId;
        newComment.createdAt = new Date();
        newComment.updatedAt = new Date();
        newComment.user = await this.usersService.getCurrentUser(request);
        return await this.commentsRepository.save(newComment);
    }
    async postComment(body) {
        const newComment = new comments_entity_1.Comments();
        newComment.content = body.content;
        newComment.contentType = body.content;
        newComment.replyToId = body.replyToId;
        newComment.createdAt = new Date();
        newComment.updatedAt = new Date();
        newComment.user = await this.usersService.getUserByID(body.authorId);
        return await this.commentsRepository.save(newComment);
    }
    async getComments() {
        return await this.commentsRepository.find();
    }
    async getCommentsOfUser(id, query) {
        const newQuery = this.commentsRepository.createQueryBuilder('comments').where('author_id = :id', { id });
        if (query.limit)
            newQuery.limit(query.limit);
        if (query.offset && query.limit)
            newQuery.offset(query.offset);
        return await newQuery.getMany();
    }
};
CommentsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => users_service_1.UsersService))),
    __param(1, (0, typeorm_1.InjectRepository)(comments_entity_1.Comments)),
    __metadata("design:paramtypes", [users_service_1.UsersService, typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object])
], CommentsService);
exports.CommentsService = CommentsService;
//# sourceMappingURL=comments.service.js.map