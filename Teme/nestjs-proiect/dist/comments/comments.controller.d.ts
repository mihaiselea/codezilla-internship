import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { GetUserDTO } from 'src/users/dtos/get-user.dto';
import { CommentsService } from './comments.service';
import { PublishCommentDTO } from './dto/publish-comment.dto';
export declare class CommentsController {
    private readonly commentService;
    constructor(commentService: CommentsService);
    getComments(): Promise<import("./comments.entity").Comments[]>;
    getCommentsOfUser(params: GetUserDTO, query: PaginatorQueryDTO): Promise<import("./comments.entity").Comments[]>;
    publishComment(request: Request, body: PublishCommentDTO): Promise<import("./comments.entity").Comments>;
}
