"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetCommentsOfUserDTO = void 0;
var gender;
(function (gender) {
    gender["MASCULIN"] = "masculin";
    gender["FEMININ"] = "feminin";
})(gender || (gender = {}));
;
class GetCommentsOfUserDTO {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
exports.GetCommentsOfUserDTO = GetCommentsOfUserDTO;
//# sourceMappingURL=get-user-comments.dto.js.map