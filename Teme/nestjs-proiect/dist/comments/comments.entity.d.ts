import { Users } from "src/users/users.entity";
export declare class Comments {
    id: number;
    replyToId: number;
    content: string;
    contentType: string;
    createdAt: Date;
    updatedAt: Date;
    user: Users;
}
