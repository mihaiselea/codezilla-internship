export declare class PostCommentsDTO {
    replyToId: number;
    content: string;
    contentType: string;
    authorId: number;
}
