import { CommentsDTO } from "./comments.dto";
declare enum gender {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
export declare class GetCommentsOfUserDTO {
    id: number;
    name: string;
    age: number;
    gender: gender;
    comments: CommentsDTO[];
    constructor(partial: Partial<GetCommentsOfUserDTO>);
}
export {};
