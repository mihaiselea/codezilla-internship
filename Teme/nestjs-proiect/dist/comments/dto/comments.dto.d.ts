export declare class CommentsDTO {
    id: number;
    userID: number;
    blogPostId: number;
    comment: string;
}
