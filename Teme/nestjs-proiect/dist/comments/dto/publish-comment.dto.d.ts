export declare class PublishCommentDTO {
    replyToId: number;
    content: string;
    contentType: string;
}
