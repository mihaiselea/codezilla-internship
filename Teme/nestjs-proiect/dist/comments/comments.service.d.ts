import { Request } from 'express';
import { PaginatorQueryDTO } from 'src/dtos/paginator-query.dto';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { Comments } from './comments.entity';
import { PostCommentsDTO } from './dto/post-comments.dto';
import { PublishCommentDTO } from './dto/publish-comment.dto';
export declare class CommentsService {
    private usersService;
    private commentsRepository;
    publishComment(body: PublishCommentDTO, request: Request): Promise<Comments>;
    constructor(usersService: UsersService, commentsRepository: Repository<Comments>);
    postComment(body: PostCommentsDTO): Promise<Comments>;
    getComments(): Promise<Comments[]>;
    getCommentsOfUser(id: number, query: PaginatorQueryDTO): Promise<Comments[]>;
}
