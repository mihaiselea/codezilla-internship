"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationService = void 0;
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const constants_1 = require("../constants");
let AuthenticationService = class AuthenticationService {
    constructor(usersSerivce) {
        this.usersSerivce = usersSerivce;
    }
    async getAccessToken(request) {
        const user = this.usersSerivce.getCurrentUser(request);
        if (user) {
            const shortUser = {
                id: user.id,
                email: user.email,
            };
            const accesToken = jwt.sign(Object.assign({}, shortUser), constants_1.Constants.secretToken, {
                expiresIn: 60 * 60,
            });
            const refreshToken = jwt.sign(Object.assign({}, shortUser), constants_1.Constants.secretToken, {
                expiresIn: '24h',
            });
            return Object.assign(Object.assign({}, shortUser), { accesToken,
                refreshToken });
        }
    }
    async userSignIn(body) {
        const user = await this.usersSerivce.getUserByEmail(body.email);
        if (user) {
            const isValidPassord = await bcrypt.compare(body.password, user.password);
            if (isValidPassord) {
                const shortUser = {
                    id: user.id,
                    email: user.email,
                };
                const accesToken = jwt.sign(Object.assign({}, shortUser), constants_1.Constants.secretToken, {
                    expiresIn: 60 * 60,
                });
                const refreshToken = jwt.sign(Object.assign({}, shortUser), constants_1.Constants.secretToken, {
                    expiresIn: '24h',
                });
                return Object.assign(Object.assign({}, shortUser), { accesToken,
                    refreshToken });
            }
        }
        throw new common_1.BadRequestException('Invalid Credentials');
    }
    async userSignup(body) {
        const salt = await bcrypt.genSalt();
        const password = await bcrypt.hash(body.password, salt);
        const user = this.usersSerivce.signUpUser(body, salt, password);
    }
};
AuthenticationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map