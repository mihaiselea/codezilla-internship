import { Strategy } from 'passport-jwt';
import { UsersService } from 'src/users/users.service';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly usersService;
    constructor(usersService: UsersService);
    validate(payload: any): Promise<import("../users/users.entity").Users>;
}
declare const JwtStrategyRefreshToken_base: new (...args: any[]) => Strategy;
export declare class JwtStrategyRefreshToken extends JwtStrategyRefreshToken_base {
    private readonly usersService;
    constructor(usersService: UsersService);
    validate(payload: any): Promise<import("../users/users.entity").Users>;
}
export {};
