import { UsersService } from 'src/users/users.service';
import { SignUpDTO } from './dto/signup.dto';
import { SignInDTO } from './dto/signin.dto';
import { Request } from 'express';
export declare class AuthenticationService {
    private readonly usersSerivce;
    getAccessToken(request: Request): Promise<{
        accesToken: string;
        refreshToken: string;
        id: any;
        email: any;
    }>;
    userSignIn(body: SignInDTO): Promise<{
        accesToken: string;
        refreshToken: string;
        id: number;
        email: string;
    }>;
    userSignup(body: SignUpDTO): Promise<void>;
    constructor(usersSerivce: UsersService);
}
