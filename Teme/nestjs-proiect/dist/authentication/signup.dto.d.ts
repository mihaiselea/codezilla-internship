export declare class SignupDTO {
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
}
