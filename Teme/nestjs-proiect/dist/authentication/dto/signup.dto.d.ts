export declare class SignUpDTO {
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
}
