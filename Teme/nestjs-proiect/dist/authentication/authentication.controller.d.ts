import { AuthenticationService } from './authentication.service';
import { SignInDTO } from './dto/signin.dto';
import { SignUpDTO } from './dto/signup.dto';
import { Request } from 'express';
export declare class AuthenticationController {
    private readonly authenticationService;
    constructor(authenticationService: AuthenticationService);
    userSignup(body: SignUpDTO): Promise<void>;
    userSignIn(body: SignInDTO): Promise<{
        accesToken: string;
        refreshToken: string;
        id: number;
        email: string;
    }>;
    getAccessToken(request: Request): Promise<{
        accesToken: string;
        refreshToken: string;
        id: any;
        email: any;
    }>;
}
