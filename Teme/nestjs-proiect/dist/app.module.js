"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const users_module_1 = require("./users/users.module");
const blog_posts_module_1 = require("./blog_posts/blog_posts.module");
const comments_module_1 = require("./comments/comments.module");
const typeorm_1 = require("@nestjs/typeorm");
const authentication_module_1 = require("./authentication/authentication.module");
const jwt_strategy_1 = require("./authentication/jwt.strategy");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [users_module_1.UsersModule, blog_posts_module_1.BlogPostsModule, comments_module_1.CommentsModule,
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'root',
                password: 'root',
                database: 'internship',
                autoLoadEntities: true,
                synchronize: true,
            }),
            authentication_module_1.AuthenticationModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, jwt_strategy_1.JwtStrategy, jwt_strategy_1.JwtStrategyRefreshToken],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map