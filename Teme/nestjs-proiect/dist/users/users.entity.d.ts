import { BlogPosts } from "src/blog_posts/blog_posts.entity";
import { Comments } from "src/comments/comments.entity";
export declare class Users {
    id: number;
    firstame: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
    salt: string;
    role: string;
    profilePicture: string;
    createdAt: Date;
    updatedAt: Date;
    blogPosts: BlogPosts[];
    comments: Comments;
}
