import { BlogPostsDTO } from "src/blog_posts/dtos/blog-posts.dto";
import { CommentsDTO } from "src/comments/dto/comments.dto";
declare enum gender {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
export declare class GetUserCommentsAndBlogsDTO {
    id: number;
    name: string;
    age: number;
    gender: gender;
    comments: CommentsDTO[];
    blogs: BlogPostsDTO[];
}
export {};
