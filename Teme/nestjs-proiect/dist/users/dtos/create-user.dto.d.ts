export declare enum UserRoles {
    DEFAULT = "user",
    ADMIN = "admin",
    PUBLISHER = "publisher"
}
export declare class CreateUserDTO {
    id: number;
    firstame: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
    salt: string;
    role: UserRoles;
    profilePicture: string;
    createdAt: Date;
    updatedAt: Date;
}
