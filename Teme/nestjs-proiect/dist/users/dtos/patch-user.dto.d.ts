import { UserRoles } from "./create-user.dto";
export declare class PatchUserDTO {
    firstame: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
    role: UserRoles;
    profilePicture: string;
}
