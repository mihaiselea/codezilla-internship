import { UserRoles } from "./create-user.dto";
export declare class ChangeUserRoleDTO {
    role: UserRoles;
}
