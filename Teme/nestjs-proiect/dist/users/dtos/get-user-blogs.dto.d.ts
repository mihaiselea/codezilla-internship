import { BlogPostsDTO } from "src/blog_posts/dtos/blog-posts.dto";
declare enum gender {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
export declare class getUserBlogsDTO {
    id: number;
    name: string;
    age: number;
    gender: gender;
    blogs: BlogPostsDTO[];
}
export {};
