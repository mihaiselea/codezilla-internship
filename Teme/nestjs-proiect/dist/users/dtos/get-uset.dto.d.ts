declare enum gender {
    MASCULIN = "masculin",
    FEMININ = "feminin"
}
export declare class GetUserDTO {
    id: number;
    name: string;
    age: number;
    gender: gender;
    constructor(partial: Partial<GetUserDTO>);
}
export {};
