export declare class PutUserDTO {
    firstame: string;
    lastName: string;
    fullName: string;
    email: string;
    password: string;
    salt: string;
    role: string;
    profilePicture: string;
}
