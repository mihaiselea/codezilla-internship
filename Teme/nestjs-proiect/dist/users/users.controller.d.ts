import { Request } from 'express';
import { ChangeUserRoleDTO } from './dtos/change-role-user.dto';
import { CreateUserDTO } from './dtos/create-user.dto';
import { GetUserDTO } from './dtos/get-user.dto';
import { PutUserDTO } from './dtos/put-user.dto';
import { UsersService } from './users.service';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    getCurrentUser(request: Request): Promise<any>;
    changeUserRole(params: GetUserDTO, body: ChangeUserRoleDTO, request: Request): Promise<import("typeorm").UpdateResult>;
    getCurrentUserByRefreshToken(request: Request): Promise<any>;
    getUsers(): Promise<import("./users.entity").Users[]>;
    getUserByID(params: GetUserDTO): Promise<import("./users.entity").Users>;
    createUser(body: CreateUserDTO): Promise<import("./users.entity").Users>;
    deleteUser(params: GetUserDTO): Promise<import("typeorm").DeleteResult>;
    patchUser(params: GetUserDTO, body: PutUserDTO): Promise<void>;
    getUserCommentsAndBlogs(params: GetUserDTO): Promise<import("./users.entity").Users>;
}
