"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const blog_posts_service_1 = require("../blog_posts/blog_posts.service");
const comments_service_1 = require("../comments/comments.service");
const typeorm_2 = require("typeorm");
const create_user_dto_1 = require("./dtos/create-user.dto");
const users_entity_1 = require("./users.entity");
let UsersService = class UsersService {
    constructor(commentsService, blogPostsService, usersRepository) {
        this.commentsService = commentsService;
        this.blogPostsService = blogPostsService;
        this.usersRepository = usersRepository;
    }
    async changeUserRole(id, body, request) {
        const admin = await this.getCurrentUser(request);
        if (admin.role != create_user_dto_1.UserRoles.ADMIN) {
            throw new common_1.UnauthorizedException();
        }
        else {
            if (body.role) {
                return await this.usersRepository.createQueryBuilder()
                    .update(users_entity_1.Users)
                    .set({ role: body.role, updatedAt: Date() })
                    .where("id = :id", { id: id })
                    .execute();
            }
        }
    }
    async getUserByEmail(email) {
        return await this.usersRepository.findOne({ email: email });
    }
    async signUpUser(body, salt, password) {
        const newUser = new users_entity_1.Users();
        newUser.firstame = body.firstName;
        newUser.lastName = body.lastName;
        newUser.fullName = body.fullName;
        newUser.email = body.email;
        newUser.password = password;
        newUser.salt = salt;
        newUser.role = create_user_dto_1.UserRoles.DEFAULT;
        newUser.profilePicture = "";
        newUser.createdAt = new Date();
        newUser.updatedAt = new Date();
        return await this.usersRepository.save(newUser);
    }
    async getUserCommentsAndBlogs(id) {
        return await this.usersRepository.findOne(id, {
            relations: ['blogPosts', 'comments']
        });
    }
    async putUser(id, body) {
        await this.usersRepository.createQueryBuilder()
            .update(users_entity_1.Users)
            .set({
            firstame: body.firstame,
            lastName: body.lastName,
            fullName: body.fullName,
            email: body.email,
            password: body.password,
            salt: body.salt,
            role: body.role,
            profilePicture: body.profilePicture,
            updatedAt: new Date,
        })
            .where({ id: id })
            .execute();
    }
    async deleteUser(id) {
        return await this.usersRepository.delete({ id: id });
    }
    async createUser(body) {
        const newUser = new users_entity_1.Users();
        newUser.firstame = body.firstame;
        newUser.lastName = body.lastName;
        newUser.fullName = body.fullName;
        newUser.email = body.email;
        newUser.password = body.password;
        newUser.salt = body.salt;
        newUser.role = body.role;
        newUser.profilePicture = body.profilePicture;
        newUser.createdAt = new Date();
        newUser.updatedAt = new Date();
        return await this.usersRepository.save(newUser);
    }
    async getUserByID(id) {
        return await this.usersRepository.findOne({ id: id });
    }
    async getUsers() {
        return await this.usersRepository.find();
    }
    async getCurrentUser(request) {
        const user = Object.assign({}, request.user);
        delete user.password;
        delete user.salt;
        return user;
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => comments_service_1.CommentsService))),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => blog_posts_service_1.BlogPostsService))),
    __param(2, (0, typeorm_1.InjectRepository)(users_entity_1.Users)),
    __metadata("design:paramtypes", [comments_service_1.CommentsService,
        blog_posts_service_1.BlogPostsService, typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map