import { Request } from 'express';
import { SignUpDTO } from 'src/authentication/dto/signup.dto';
import { BlogPostsService } from 'src/blog_posts/blog_posts.service';
import { CommentsService } from 'src/comments/comments.service';
import { Repository } from 'typeorm';
import { ChangeUserRoleDTO } from './dtos/change-role-user.dto';
import { CreateUserDTO } from './dtos/create-user.dto';
import { PutUserDTO } from './dtos/put-user.dto';
import { Users } from './users.entity';
export declare class UsersService {
    private commentsService;
    private blogPostsService;
    private usersRepository;
    changeUserRole(id: number, body: ChangeUserRoleDTO, request: Request): Promise<import("typeorm").UpdateResult>;
    getUserByEmail(email: string): Promise<Users>;
    signUpUser(body: SignUpDTO, salt: any, password: any): Promise<Users>;
    getUserCommentsAndBlogs(id: number): Promise<Users>;
    putUser(id: number, body: PutUserDTO): Promise<void>;
    deleteUser(id: number): Promise<import("typeorm").DeleteResult>;
    createUser(body: CreateUserDTO): Promise<Users>;
    getUserByID(id: number): Promise<Users>;
    getUsers(): Promise<Users[]>;
    getCurrentUser(request: Request): Promise<any>;
    constructor(commentsService: CommentsService, blogPostsService: BlogPostsService, usersRepository: Repository<Users>);
}
