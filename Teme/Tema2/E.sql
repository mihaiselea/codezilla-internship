/*
-- Query: SELECT customer_id, COUNT(*) as count FROM payment group by customer_id having count >= 30 order by count DESC LIMIT 15 OFFSET 5
-- Date: 2022-03-04 13:01
*/
INSERT INTO `` (`customer_id`,`count`) VALUES (197,40);
INSERT INTO `` (`customer_id`,`count`) VALUES (469,40);
INSERT INTO `` (`customer_id`,`count`) VALUES (178,39);
INSERT INTO `` (`customer_id`,`count`) VALUES (468,39);
INSERT INTO `` (`customer_id`,`count`) VALUES (137,39);
INSERT INTO `` (`customer_id`,`count`) VALUES (5,38);
INSERT INTO `` (`customer_id`,`count`) VALUES (295,38);
INSERT INTO `` (`customer_id`,`count`) VALUES (410,38);
INSERT INTO `` (`customer_id`,`count`) VALUES (459,38);
INSERT INTO `` (`customer_id`,`count`) VALUES (176,37);
INSERT INTO `` (`customer_id`,`count`) VALUES (257,37);
INSERT INTO `` (`customer_id`,`count`) VALUES (198,37);
INSERT INTO `` (`customer_id`,`count`) VALUES (366,37);
INSERT INTO `` (`customer_id`,`count`) VALUES (354,36);
INSERT INTO `` (`customer_id`,`count`) VALUES (439,36);
